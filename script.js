let tabs = document.querySelectorAll('.tabs-title');
let text = document.querySelectorAll('.tabs-content-text');

tabs.forEach(tab => {
    tab.addEventListener('click', () => {
        tabs.forEach(t => {
            t.classList.remove('active');
        });
        tab.classList.add('active');
        text.forEach(elem => {
            elem.classList.remove('active')
            if(tab.dataset.name === elem.dataset.name) {
                elem.classList.add('active');
            }
        });
    });
});
